<?php include './components/mysql_pdo.php'; ?>

<?php include './components/log.php' ?>

<?php
//Code traitement
session_start();
?>

<?php 
    $title="Pixie : Mise à jour du mot de passe";
    include './components/header.php'; 
?>

<?php include './components/navbar.php'; ?>

<?php include './components/content.php'; ?>

<div class="d-flex align-items-center justify-content-center w-100">
    <div class="row justify-content-center w-100">
        <div class="col-md-8 col-lg-6 col-xxl-3">
            <div class="card mb-0">
                <div class="card-body">
                    <a href="./index.php" class="text-nowrap logo-img text-center d-block py-3 w-100">
                        <img src="./assets/images/logos/logo.png" width="180" alt="">
                    </a>
                    <p class="text-center">Pixie : MAJ du mot de passe</p>
                    <form action="maj_mdp.php" method="post">
                        <div class="mb-4">
                        <label for="mdp" class="form-label">Votre nouveau mot de passe</label>
                        <input type="password" class="form-control" id="mdp" name="mdp">
                        </div>
                        <button type="submit" class="btn btn-primary w-100 py-8 fs-4 mb-4 rounded-2">Mettre à jour et accéder à l'espace</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include './components/footer.php'; ?>