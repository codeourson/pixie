<?php include './components/config.php';?>

<!doctype html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $title?></title>
  <?php echo '<link rel="shortcut icon" type="image/png" href="'. $config['PIXIE']['DOSSIER_PIXIE'] .'/assets/images/logos/favicon.png" />'; ?>
  <?php echo '<link rel="stylesheet" href="'. $config['PIXIE']['DOSSIER_PIXIE'] .'/assets/css/styles.min.css" />'; ?>
  <?php echo '<link rel="stylesheet" href="'. $config['PIXIE']['DOSSIER_PIXIE'] .'/assets/css/custom.css" />'; ?>
  <?php echo '<script src="'. $config['PIXIE']['DOSSIER_PIXIE'] .'/assets/libs/jquery/dist/jquery.min.js"></script>'; ?>
  <?php include './components/scriptsjs.php'; ?>
</head>

<body>
  <!--  Body Wrapper -->
  <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed">