<?php
// Inclusion du fichier d'autoloading de SimpleSAMLphp
require_once '/var/simplesamlphp/lib/_autoload.php';

// Initialisation de SimpleSAMLphp
$auth = new \SimpleSAML\Auth\Simple('default-sp');

// Vérification si l'utilisateur est déjà authentifié
if ($auth->isAuthenticated()) 
    {
    // L'utilisateur est authentifié
    // Vous pouvez récupérer les attributs de l'utilisateur authentifié
    $attributes = $auth->getAttributes();
    } 
else 
    {
    // L'utilisateur n'est pas authentifié
    // Redirigez l'utilisateur vers la page d'authentification
    $auth->login();
    }


/*//AUTH Traditionnel
if (!isset($_SESSION['utilisateurs'])) 
	{
    //Redirection vers le login
    header("Location: index.php");
	} */
?>