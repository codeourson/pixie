<?php
session_start();
$utilisateurs = $_SESSION['utilisateurs'];
?>

<?php include './components/mysql_pdo.php'; ?>

<?php include './components/auth.php' ?>

<?php include './components/log.php' ?>

<?php
$title = "Polaris : Le Bottin";
include './components/header.php';
?>

<?php include './components/navbar.php'; ?>

<?php include './components/content.php'; ?>

<div style="position:fixed;background-color:#fcfcfc;width:100vw">
<label for="itemsPerPage">Éléments par page:</label>
<select id="itemsPerPage" name="itemsPerPage">
    <option value="5">5</option>
    <option value="10" selected>10</option>
    <option value="25">25</option>
    <option value="50">50</option>
    <option value="100">100</option>
</select>

<label for="search">Rechercher :</label>
<input type="text" id="search" name="search">
<button id="searchBtn" class="btn btn-primary">Rechercher</button>

<label class="form-check-label" for="disableSearch"><input class="form-check-input" type="checkbox" id="disableSearch" name="disableSearch" style="margin-left:20px;">Rechercher les anomalies</label>

<br><br>
<label for="filters">Filtres de recherche :</label>
<div id="filters" class="checkbox-container row row-cols-2 row-cols-sm-3 row-cols-md-4 row-cols-lg-5 row-cols-xl-5" style="width:80vw">

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleNoms" checked onchange="toggleColumn('noms')">
        <label class="form-check-label" for="toggleNoms">Nom</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="togglePrenoms" checked onchange="toggleColumn('prenoms')">
        <label class="form-check-label" for="togglePrenoms">Prenom</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleId_niveaux_encadrements" checked onchange="toggleColumn('id_niveaux_encadrements')">
        <label class="form-check-label" for="toggleId_niveaux_encadrements">Encadrement</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleId_fonctions" checked onchange="toggleColumn('id_fonctions')">
        <label class="form-check-label" for="toggleId_fonctions">Fonction</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleId_contrats" checked onchange="toggleColumn('id_contrats')">
        <label class="form-check-label" for="toggleId_contrats">Contrat</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleId_services" checked onchange="toggleColumn('id_services')">
        <label class="form-check-label" for="toggleId_services">Service</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleResponsables" checked onchange="toggleColumn('responsables')">
        <label class="form-check-label" for="toggleResponsables">Responsables</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleId_secteurs" checked onchange="toggleColumn('id_secteurs')">
        <label class="form-check-label" for="toggleId_secteurs">Secteur</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleId_sites" checked onchange="toggleColumn('id_sites')">
        <label class="form-check-label" for="toggleId_sites">Site</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleStatuts" checked onchange="toggleColumn('statuts')">
        <label class="form-check-label" for="toggleStatus">Status</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleMissions_rh" checked onchange="toggleColumn('missions_rh')">
        <label class="form-check-label" for="toggleMissions_rh">Missions RH</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" width="100" type="checkbox" id="toggleSexes" checked onchange="toggleColumn('sexes')">
        <label class="form-check-label" for="toggleSexes">Sexe</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleCode" checked onchange="toggleColumn('code')">
        <label class="form-check-label" for="toggleCode">Code</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleCourriels" checked onchange="toggleColumn('courriels')">
        <label class="form-check-label" for="toggleCourriels">Courriel</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="togglePhotos" checked onchange="toggleColumn('photos')">
        <label class="form-check-label" for="togglePhotos">Photo</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleUtilisateurs" checked onchange="toggleColumn('utilisateurs')">
        <label class="form-check-label" for="toggleUtilisateurs">Utilisateur</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleTelephones_sites" checked onchange="toggleColumn('telephones_sites')">
        <label class="form-check-label" for="toggleTelephones_sites">N° Site</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleTelephones_poste" checked onchange="toggleColumn('telephones_poste')">
        <label class="form-check-label" for="toggleTelephones_poste">N° Poste</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleLignes_directes" checked onchange="toggleColumn('lignes_directes')">
        <label class="form-check-label" for="toggleLignes_directes">N° Directe</label>
    </div>

    <div class="col form-check form-switch" style="padding-right:50px">
        <input class="form-check-input" type="checkbox" id="toggleTelephones_portables" checked onchange="toggleColumn('telephones_portables')">
        <label class="form-check-label" for="toggleTelephones_portables">N° Portable</label>
    </div>
</div>
</div>
<script>
    function loadFilters() {
        var filters = localStorage.getItem('columnFilters');
        if (filters) {
            columnFilters = JSON.parse(filters);
            applyFilters();
            // Appliquer les états des cases à cocher
            for (var column in columnFilters) {
                $('#toggle' + column.charAt(0).toUpperCase() + column.slice(1)).prop('checked', columnFilters[column]);
            }
        }
    }

    // Variable pour stocker l'état des filtres checkbox
    var columnFilters = {
        'code': true,
        'noms' : true,
        'prenoms' : true,
        'sexes' : true,
        'photos' : true,
        'statuts' : true,
        'id_fonctions' : true,
        'id_contrats' : true,
        'courriels' : true,
        'utilisateurs' : true,
        'secteurs' : true,
        'sites' : true,
        'services' : true,
        'responsables' : true
        };

    // Fonction pour sauvegarder les filtres dans localStorage
    function saveFilters() {
        localStorage.setItem('columnFilters', JSON.stringify(columnFilters));
    }

    function toggleColumn(columnName) {
        var elements = document.getElementsByClassName(columnName);
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].style.display === 'none') {
                elements[i].style.display = '';
                columnFilters[columnName] = true;
            } else {
                elements[i].style.display = 'none';
                columnFilters[columnName] = false;
            }
        }
        saveFilters(); // Sauvegarder les filtres à chaque changement
    }

    // Fonction pour appliquer l'état des filtres après une recherche
    function applyFilters() {
        for (var column in columnFilters) {
            var elements = document.getElementsByClassName(column);
            for (var i = 0; i < elements.length; i++) {
                if (columnFilters[column]) {
                    elements[i].style.display = '';
                } else {
                    elements[i].style.display = 'none';
                }
            }
        }
    }

    // Appeler la fonction loadFilters() au chargement de la page
    loadFilters();
</script>

<script>
$(document).ready(function(){
    // Variable pour stocker le nombre total de résultats et le nombre total de pages
    var totalResults = 0;
    var totalPages = 0;

    // Fonction pour charger les données du tableau
    function loadData(page, itemsPerPage, searchTerm) {
        $.ajax({
            url: 'polaris_app_001_get_data.php',
            type: 'GET',
            data: { page: page, itemsPerPage: itemsPerPage, searchTerm: searchTerm },
            success: function(response) {
                $('#tableinlinediv').html(response);
                updatePagination(page, itemsPerPage, searchTerm);
                applyFilters(); // Appel à applyFilters une fois que les données sont chargées et affichées
            }
        });
    }

    // Charger les données initiales
    loadData(1, $('#itemsPerPage').val(), '');
    applyFilters();

    // Gérer le changement d'état de la case à cocher
    $('#disableSearch').change(function() {
    var isChecked = $(this).is(':checked');
    $('#search').prop('disabled', isChecked); // Désactiver le champ de recherche selon l'état de la case à cocher
    console.log(isChecked);
    if(isChecked==true)
        {
        $('#page1').val(1);
        $('#page2').val(1);
        $('#search').val(999999);
        var searchTerm = $('#search').val();
        loadData(1, $('#itemsPerPage').val(), searchTerm);
        }
    else
        {
        $('#page1').val(1);
        $('#page2').val(1);
        $('#search').val('');
        var searchTerm = $('#search').val();
        loadData(1, $('#itemsPerPage').val(), searchTerm);
        }
    });

    // Gérer les changements dans le champ de recherche et déclencher la recherche
    $('#searchBtn').click(function() {
        var searchTerm = $('#search').val();
        loadData(1, $('#itemsPerPage').val(), searchTerm);
        applyFilters();
    });

    // Gérer les changements de page pour le premier bloc
    $('#prevPage1').click(function() {
        var page = parseInt($('#page1').val());
        if (page > 1) {
            var searchTerm = $('#search').val();
            loadData(page - 1, $('#itemsPerPage').val(), searchTerm);
            $('#page1').val(page - 1);
            $('#page2').val(page - 1);
        }
        applyFilters();
    });

    // Écouter les changements dans le champ de saisie de la première page
    $('#page1').on('input', function() {
        var page = $(this).val();
        $('#page2').val(page); // Mettre à jour le champ de saisie de la deuxième page
    });

    // Écouter les changements dans le champ de saisie de la deuxième page
    $('#page2').on('input', function() {
        var page = $(this).val();
        $('#page1').val(page); // Mettre à jour le champ de saisie de la première page
    });

    $('#nextPage1').click(function() {
        var page = parseInt($('#page1').val());
        var searchTerm = $('#search').val();
        applyFilters();
        loadData(page + 1, $('#itemsPerPage').val(), searchTerm);
        $('#page1').val(page + 1);
        $('#page2').val(page + 1);
    });

    // Gérer les changements de page pour le deuxième bloc
    $('#prevPage2').click(function() {
        var page = parseInt($('#page2').val());
        if (page > 1) {
            var searchTerm = $('#search').val();
            loadData(page - 1, $('#itemsPerPage').val(), searchTerm);
            $('#page1').val(page - 1);
            $('#page2').val(page - 1);
        }
        applyFilters();
    });

    $('#nextPage2').click(function() {
        var page = parseInt($('#page2').val());
        var searchTerm = $('#search').val();
        applyFilters();
        loadData(page + 1, $('#itemsPerPage').val(), searchTerm);
        $('#page1').val(page + 1);
        $('#page2').val(page + 1);
    });

    // Soumettre le formulaire pour aller à une page spécifique pour le premier bloc
    $('#pageForm1').submit(function(e) {
        e.preventDefault();
        var page = parseInt($('#page1').val());
        var searchTerm = $('#search').val();
        loadData(page, $('#itemsPerPage').val(), searchTerm);
        applyFilters();
    });

    // Soumettre le formulaire pour aller à une page spécifique pour le deuxième bloc
    $('#pageForm2').submit(function(e) {
        e.preventDefault();
        var page = parseInt($('#page2').val());
        var searchTerm = $('#search').val();
        loadData(page, $('#itemsPerPage').val(), searchTerm);
        applyFilters();
    });

    // Gérer les changements de nombre d'éléments par page
    $('#itemsPerPage').change(function() {
        var searchTerm = $('#search').val();
        loadData(1, $(this).val(), searchTerm);
        applyFilters();
    });

    
    function updatePagination(page, itemsPerPage, searchTerm) {
    // Envoyer une autre requête AJAX pour obtenir le nombre total d'éléments
    $.ajax({
        url: 'polaris_app_001_get_total_count.php', // Remplacez par le nom du script PHP qui récupère le nombre total d'éléments
        type: 'GET',
        data: { searchTerm: searchTerm },
        success: function(totalCount) {
            var itemsPerPage = $('#itemsPerPage').val();
            var totalPages = Math.ceil(totalCount / itemsPerPage);
            // Mettre à jour les boutons précédents et suivants en fonction de la nouvelle valeur de totalPages
            if (page > 1) {
                $('#prevPage1').prop('disabled', false);
                $('#prevPage2').prop('disabled', false);
            } else {
                $('#prevPage1').prop('disabled', true);
                $('#prevPage2').prop('disabled', true);
            }
            if (page < totalPages) {
                $('#nextPage1').prop('disabled', false);
                $('#nextPage2').prop('disabled', false);
            } else {
                $('#nextPage1').prop('disabled', true);
                $('#nextPage2').prop('disabled', true);
            }
        }
    });
}

    // Gérer les changements de sélection (select)
    $(document).on('change', '.selectable', function(){
        var id = $(this).data('id');
        var field = $(this).data('field');
        var value = $(this).val();
        $.ajax({
            url: 'polaris_app_001_update_data.php',
            type: 'POST',
            data: { id: id, field: field, value: value },
            success: function(response) {
                // Afficher un message de succès ou de gestion des erreurs
                console.log(response);
                applyFilters();
                var page = parseInt($('#page1').val());
                var searchTerm = $('#search').val();
                loadData(page, $('#itemsPerPage').val(), searchTerm);
            }
        });
    });

    // Gérer le téléchargement d'image
    $(document).on('change', '.image-upload', function(){
        var id = $(this).data('id');
        var formData = new FormData();
        formData.append('id', id);
        formData.append('image', $(this)[0].files[0]);
        $.ajax({
            url: 'polaris_app_001_upload_image.php',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                // Afficher un message de succès ou de gestion des erreurs
                console.log(response);
                applyFilters();
                var page = parseInt($('#page1').val());
                var searchTerm = $('#search').val();
                loadData(page, $('#itemsPerPage').val(), searchTerm);
            }
        });
    });

    // Gérer les changements de sélection (cases à cocher)
    $(document).on('change', '.checkbox', function(){
        var id = $(this).data('id');
        var field = $(this).data('field');
        var checkedValues = []; // Tableau pour stocker les valeurs des cases cochées
        $('.checkbox:checked').each(function() {
            checkedValues.push($(this).val());
        });
        
        // Envoyer les valeurs via AJAX
        $.ajax({
            url: 'polaris_app_001_update_data.php',
            type: 'POST',
            data: { id: id, field: field, value: checkedValues }, // Envoyer le tableau des valeurs des cases cochées
            success: function(response) {
                // Afficher un message de succès ou de gestion des erreurs
                console.log(response);
                applyFilters();
                var page = parseInt($('#page1').val());
                var searchTerm = $('#search').val();
                loadData(page, $('#itemsPerPage').val(), searchTerm);
            }
        });
    });

    // Fonction pour mettre à jour les données
    $(document).on('blur', '.editable', function(){
        var id = $(this).data('id');
        var field = $(this).data('field');
        var value;
        if ($(this).is('textarea')) {
            value = $(this).val();
        } else {
            value = $(this).text();
        }
        $.ajax({
            url: 'polaris_app_001_update_data.php',
            type: 'POST',
            data: { id: id, field: field, value: value },
            success: function(response) {
                // Afficher un message de succès ou de gestion des erreurs
                console.log(response);
            }
        });
        applyFilters();
    });
});


</script>

<div id="pagination1" style="padding-top:250px">
    <form id="pageForm1">
    <button id="prevPage1" class="btn btn-primary">Précédent</button>
    <button id="nextPage1" class="btn btn-primary">Suivant</button>
        <label for="page1">Aller à la page :</label>
        <input type="number" id="page1" name="page1" min="1" value="1">
        <input type="submit" class="btn btn-primary" value="Go">
    </form>
</div>

<div class="tableinline table-responsive" id="tableinlinediv" style="overflow-x: auto;">
</div>

<br>
<div id="pagination2">
    <form id="pageForm2">
    <button id="prevPage2" class="btn btn-primary">Précédent</button>
    <button id="nextPage2" class="btn btn-primary">Suivant</button>
        <label for="page2">Aller à la page :</label>
        <input type="number" id="page2" name="page2" min="1" value="1">
        <input type="submit" class="btn btn-primary" value="Go">
    </form>
</div>

    <script>
let isMouseDown = false;
let initialMouseX = 0;
let initialScrollLeft = 0;

document.getElementById('tableinlinediv').addEventListener('mousedown', function(event) {
  if (event.button === 0) { // Vérifie si le bouton enfoncé est le bouton gauche de la souris
    isMouseDown = true;
    initialMouseX = event.clientX;
    initialScrollLeft = this.scrollLeft;
  }
});

document.addEventListener('mouseup', function(event) {
  if (event.button === 0) { // Vérifie si le bouton relâché est le bouton gauche de la souris
    isMouseDown = false;
  }
});

document.addEventListener('mousemove', function(event) {
  if (isMouseDown) {
    const deltaX = event.clientX - initialMouseX;
    document.getElementById('tableinlinediv').scrollLeft = initialScrollLeft - deltaX;
  }
});

</script>

<?php include './components/footer.php'; ?>
