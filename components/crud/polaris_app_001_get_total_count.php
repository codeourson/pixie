<?php
include 'components/mysql_pdo.php';

$searchTerm = isset($_GET['searchTerm']) ? $_GET['searchTerm'] : '';
$columns = ['id','code','noms','prenoms','statuts','id_fonctions','photos','courriels','utilisateurs','missions_rh','id_secteurs','id_sites_principaux','id_sites_secondaires','telephones_sites','telephones_poste','lignes_directes','telephones_portables','id_services','id_niveaux_encadrements','ordres_encadrements','sexes','type_contrats','contrats','dates_debut_contrat','dates_fin_contrat','responsables','horaires_hebdomadaires','coordo1','coordo2','etats']; // Remplacez par les noms réels de vos colonnes
$searchConditions = [];
foreach ($columns as $column) {
    $searchConditions[] = "$column LIKE :searchTerm";
}

// Récupérer les données depuis la table
$query = "SELECT COUNT(*) AS total FROM `module-001-liste-employes` WHERE " . implode(' OR ', $searchConditions) . "";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':searchTerm', "%$searchTerm%", PDO::PARAM_STR);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);

$totalCount = $result['total'];

echo $totalCount;
?>