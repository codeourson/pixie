<?php
include 'components/error.php';
include 'components/mysql_pdo.php';

// Récupérer les données depuis la requête AJAX
$id = $_POST['id'];
$field = $_POST['field'];
$value = $_POST['value'];

echo $value;

// Mettre à jour la ligne dans la base de données
if($field == "id_contrats")
    {
    $sql2 = "SELECT * FROM `module-001-contrats` WHERE id LIKE '".$value."'";
    $stmt2 = $dbh->query($sql2);
    $stmt2->execute();

    $type_contrat="";
    $contrats="";

    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
        {
        $contrats=$row2['contrats'];
        $type_contrats=$row2['types_de_contrats'];
        }

    $query = "UPDATE `module-001-liste-employes` SET `type_contrats` = :val WHERE id = :id";
    $stmt = $dbh->prepare($query);
    $stmt->bindParam(':val', $type_contrats);
    $stmt->bindParam(':id', $id);
    if ($stmt->execute()) {
        echo "La requête a été exécutée avec succès.";
    } else {
        echo "Erreur lors de l'exécution de la requête : " . implode(", ", $stmt->errorInfo());
    }

    $query = "UPDATE `module-001-liste-employes` SET `contrats` = :val WHERE id = :id";
    $stmt = $dbh->prepare($query);
    $stmt->bindParam(':val', $contrats);
    $stmt->bindParam(':id', $id);
    if ($stmt->execute()) {
        echo "La requête a été exécutée avec succès.";
    } else {
        echo "Erreur lors de l'exécution de la requête : " . implode(", ", $stmt->errorInfo());
    }
    
    // Afficher un message de succès ou de gestion des erreurs
    $statut = '<div class="alert alert-success" role="alert">Mise à jour effectuée !</div>';
    }
else if($field != "id_sites_secondaires")
    {
    $query = "UPDATE `module-001-liste-employes` SET $field = :value WHERE id = :id";
    $stmt = $dbh->prepare($query);
    $stmt->bindParam(':value', $value);
    $stmt->bindParam(':id', $id);
    if ($stmt->execute()) {
        echo "La requête a été exécutée avec succès.";
    } else {
        echo "Erreur lors de l'exécution de la requête : " . implode(", ", $stmt->errorInfo());
    }

    // Afficher un message de succès ou de gestion des erreurs
    $statut = '<div class="alert alert-success" role="alert">Mise à jour effectuée !</div>';
    }
else
    {
    $data = "";
    foreach($value as $val)
        {
        $data .= $val."|";
        }

    echo $data;

    $id = $_POST['id'];
    $field = $_POST['field'];
    $value = $_POST['value'];

    $query = "UPDATE `module-001-liste-employes` SET $field = :value WHERE id = :id";
    $stmt = $dbh->prepare($query);
    $stmt->bindParam(':value', $data);
    $stmt->bindParam(':id', $id);
    if ($stmt->execute()) {
        echo "La requête a été exécutée avec succès.";
    } else {
        echo "Erreur lors de l'exécution de la requête : " . implode(", ", $stmt->errorInfo());
    }

    // Afficher un message de succès ou de gestion des erreurs
    $statut = '<div class="alert alert-success" role="alert">Mise à jour effectuée !</div>';
    }
?>