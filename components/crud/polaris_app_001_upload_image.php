<?php
include 'components/mysql_pdo.php';

// Vérifie si le fichier a été correctement envoyé
if ($_FILES['image']['error'] === UPLOAD_ERR_OK) 
    {
    // Déplace le fichier vers le répertoire souhaité
    $destination = './components/database/photos/' . $_FILES['image']['name'];
    move_uploaded_file($_FILES['image']['tmp_name'], $destination);

    // Mettre à jour la ligne dans la base de données
    $query = "UPDATE `module-001-liste-employes` SET `photos` = :value WHERE id = :id";
    echo $query;
    $stmt = $dbh->prepare($query);
    $stmt->bindParam(':value', $destination);
    $stmt->bindParam(':id', $_POST['id']);
    $stmt->execute();

    // Retourne un message de succès
    echo 'Image téléchargée avec succès.';
    } 
else 
    {
    // Retourne un message d'erreur
    echo 'Erreur lors du téléchargement de l\'image.';
    }
?>