<?php
include 'components/mysql_pdo.php';

// Récupérer le terme de recherche depuis la requête AJAX
$searchTerm = isset($_GET['searchTerm']) ? $_GET['searchTerm'] : '';

// Liste des colonnes de la table
$columns = ['id','code','noms','prenoms','statuts','id_fonctions','photos','courriels','utilisateurs','missions_rh','id_secteurs','id_sites_principaux','id_sites_secondaires','telephones_sites','telephones_poste','lignes_directes','telephones_portables','id_services','id_niveaux_encadrements','ordres_encadrements','sexes','type_contrats','contrats','dates_debut_contrat','dates_fin_contrat','responsables','horaires_hebdomadaires','coordo1','coordo2','etats']; // Remplacez par les noms réels de vos colonnes
$searchConditions = [];
foreach ($columns as $column) {
    $searchConditions[] = "$column LIKE :searchTerm";
}

// Nombre d'éléments à afficher par page
$itemsPerPage = isset($_GET['itemsPerPage']) ? intval($_GET['itemsPerPage']) : 10;
// Page actuelle
$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
// Calcul de l'offset
$offset = ($page - 1) * $itemsPerPage;

// Récupérer les données depuis la table
$query = "SELECT * FROM `module-001-liste-employes` WHERE " . implode(' OR ', $searchConditions) . " LIMIT :offset, :itemsPerPage";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':searchTerm', "%$searchTerm%", PDO::PARAM_STR);
$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
$stmt->bindValue(':itemsPerPage', $itemsPerPage, PDO::PARAM_INT);
$stmt->execute();
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

$html .= '<table class="table" style="table-layout: fixed;width: 4850px;">
            <tr>
                <th width="50px" scope="col" class="id">ID</th>
                <th width="150px" scope="col" class="code">Code SAGE</th>
                <th width="150px" scope="col" class="noms">Nom</th>
                <th width="150px" scope="col" class="prenoms">Prenoms</th>
                <th width="150px" scope="col" class="sexes">Sexe</th>
                <th width="350px" scope="col" class="photos">Photos</th>
                <th width="150px" scope="col" class="statuts">Statuts</th>
                <th width="300px" scope="col" class="id_niveaux_encadrements">Encadrements</th>
                <th width="300px" scope="col" class="id_fonctions">Fonctions</th>
                <th width="300px" scope="col" class="id_contrats">Contrats</th>
                <th width="300px" scope="col" class="id_services">Services</th>
                <th width="300px" scope="col" class="responsables">Responsables</th>
                <th width="300px" scope="col" class="coordo1">Coordinateurs 1</th>
                <th width="300px" scope="col" class="coordo2">Coordinateurs 2</th>
                <th width="300px" scope="col" class="courriels">Courriels</th>
                <th width="200px" scope="col" class="utilisateurs">Utilisateurs</th>
                <th width="450px" scope="col" class="missions_rh">Missions RH</th>
                <th width="150px" scope="col" class="id_secteurs">Secteurs</th>
                <th width="250px" scope="col" class="id_sites_principaux">Sites Principaux</th>
                <th width="850px" scope="col" class="id_sites_secondaires">Sites Secondaires</th>
                <th width="150px" scope="col" class="telephones_sites">N° Site</th>
                <th width="150px" scope="col" class="telephones_poste">N° Poste</th>
                <th width="150px" scope="col" class="lignes_directes">N° Directe</th>
                <th width="150px" scope="col" class="telephones_portables">N° Portable</th>
            </tr>';

foreach ($rows as $row) 
    {
    $html .= '<tr>';
    $html .= '<td class="id">' . $row['id'] . '</td>';
    $html .= '<td class="editable code" data-id="' . $row['id'] . '" data-field="code" contenteditable>' . $row['code'] . '</td>';
    $html .= '<td class="editable noms" data-id="' . $row['id'] . '" data-field="noms" contenteditable>' . $row['noms'] . '</td>';
    $html .= '<td class="editable prenoms" data-id="' . $row['id'] . '" data-field="prenoms" contenteditable>' . $row['prenoms'] . '</td>';
    if($row['sexes']==='Homme' || $row['sexes']==='H' || $row['sexes']==='M')
        {
        $html .='<td class="sexes"><select style="width:auto" class="form-select selectable" data-id="' . $row['id'] . '" data-field="sexes">';
        $html .='<option value="Femme">Femme</option>';
        $html .='<option value="Homme" selected>Homme</option>';
        $html .='<option value="n/A">Non défini</option>';
        $html .='</select></td>';
        }
    else if($row['sexes']==='Femme' || $row['sexes']==='F' || $row['sexes']==='W')
        {
        $html .='<td class="sexes"><select style="width:auto" class="form-select selectable" data-id="' . $row['id'] . '" data-field="sexes">';
        $html .='<option value="Femme" selected>Femme</option>';
        $html .='<option value="Homme">Homme</option>';
        $html .='<option value="N/A">Non défini</option>';
        $html .='</select></td>';
        }
    else
        {
        $html .='<td class="sexes"><select style="width:auto" class="form-select selectable" data-id="' . $row['id'] . '" data-field="sexes">';
        $html .='<option value="Femme">Femme</option>';
        $html .='<option value="Homme">Homme</option>';
        $html .='<option value="N/A" selected>Non défini</option>';
        $html .='</select></td>';
        }
    $html .='<td class="photos" data-field="photos" data-id="' . $row['id'] . '"><input type="file" class="image-upload" data-id="' . $row['id'] . '"><br><img style="width:auto;height:60px;" src="'.$row['photos'].'" /></td>';

    if($row['statuts']==='ACTIF')
        {   
        $html .='<td class="statuts"><select style="width:auto" class="form-select selectable" data-id="' . $row['id'] . '" data-field="statuts">';
        $html .='<option value="ACTIF" selected>ACTIF</option>';
        $html .='<option value="INACTIF">INACTIF</option>';
        $html .='<option value="">Non défini</option>';
        $html .='</select></td>';
        }
    else if($row['statuts']==='INACTIF')
        {   
        $html .='<td class="statuts"><select style="width:auto" class="form-select selectable" data-id="' . $row['id'] . '" data-field="statuts">';
        $html .='<option value="ACTIF">ACTIF</option>';
        $html .='<option value="INACTIF" selected>INACTIF</option>';
        $html .='<option value="">Non défini</option>';
        $html .='</select></td>';
        }
    else
        {   
        $html .='<td class="statuts"><select style="width:auto" class="form-select selectable" data-id="' . $row['id'] . '" data-field="statuts">';
        $html .='<option value="ACTIF">ACTIF</option>';
        $html .='<option value="INACTIF">INACTIF</option>';
        $html .='<option value="" selected>Non défini</option>';
        $html .='</select></td>';
        }

    $html .='<td class="id_niveaux_encadrements"><select style="width:250px" class="form-select selectable" data-id="' . $row['id'] . '" data-field="id_niveaux_encadrements">';
    $sql2 = "SELECT * FROM `module-001-niveaux-encadrement`";
    $stmt2 = $dbh->query($sql2);
    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
        {
        if($row2['id']==$row['id_niveaux_encadrements'])
            {
            $html .='<option value="'.$row2['id'].'" selected>'.$row2['niveaux_encadrement'].'</option>';    
            }
        else
            {
            $html .='<option value="'.$row2['id'].'">'.$row2['niveaux_encadrement'].'</option>';  
            }
        }

    $html .='<td class="id_fonctions"><select style="width:250px" class="form-select selectable" data-id="' . $row['id'] . '" data-field="id_fonctions">';
    if($row['sexes']==='Homme' || $row['sexes']==='H' || $row['sexes']==='M')
        {
        $sql2 = "SELECT * FROM `module-001-fonctions`";
        $stmt2 = $dbh->query($sql2);
        while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
            {
            if($row2['id']==$row['id_fonctions'])
                {
                $html .='<option value="'.$row2['id'].'" selected>'.$row2['homme'].'</option>';    
                }
            else
                {
                $html .='<option value="'.$row2['id'].'">'.$row2['homme'].'</option>';  
                }
            }
        }
    else
        {
        $sql2 = "SELECT * FROM `module-001-fonctions`";
        $stmt2 = $dbh->query($sql2);
        while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
            {
            if($row2['id']==$row['id_fonctions'])
                {
                $html .='<option value="'.$row2['id'].'" selected>'.$row2['femme'].'</option>';    
                }
            else
                {
                $html .='<option value="'.$row2['id'].'">'.$row2['femme'].'</option>';  
                }
            }
        }
    $html .='</select></td>';

    $html .='<td class="id_contrats"><select style="width:250px" class="form-select selectable" data-id="' . $row['id'] . '" data-field="id_contrats">';
    if($row['sexes']==='Homme' || $row['sexes']==='H' || $row['sexes']==='M')
        {
        $sql2 = "SELECT * FROM `module-001-contrats`";
        $stmt2 = $dbh->query($sql2);
        while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
            {
            if($row2['types_de_contrats'] == $row['type_contrats'] && $row2['contrats'] == $row['contrats'])
                {
                $html .='<option value="'.$row2['id'].'" selected>'.$row2['types_de_contrats'].' - '.$row2['contrats'].'</option>';    
                }
            else
                {
                $html .='<option value="'.$row2['id'].'">'.$row2['types_de_contrats'].' - '.$row2['contrats'].'</option>';  
                }
            }
        }
    else
        {
        $sql2 = "SELECT * FROM `module-001-fonctions`";
        $stmt2 = $dbh->query($sql2);
        while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
            {
            if($row2['id']==$row['id_fonctions'])
                {
                $html .='<option value="'.$row2['id'].'" selected>'.$row2['femme'].'</option>';    
                }
            else
                {
                $html .='<option value="'.$row2['id'].'">'.$row2['femme'].'</option>';  
                }
            }
        }
    $html .='</select></td>';

    $html .='<td class="id_services"><select style="width:250px" class="form-select selectable" data-id="' . $row['id'] . '" data-field="id_services">';
    $sql2 = "SELECT * FROM `module-001-services`";
    $stmt2 = $dbh->query($sql2);
    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
        {
        if($row2['id']==$row['id_services'])
            {
            $html .='<option value="'.$row2['id'].'" selected>'.$row2['services_rh'].'</option>';    
            }
        else
            {
            $html .='<option value="'.$row2['id'].'">'.$row2['services_rh'].'</option>';  
            }
        }
    $html .='</select></td>';

    $html .='<td class="responsables"><select style="width:250px" class="form-select selectable" data-id="' . $row['id'] . '" data-field="responsables">';
    $sql2 = "SELECT codes FROM `module-001-responsables-encadrement` WHERE `responsables` LIKE 'true'";
    $stmt2 = $dbh->query($sql2);
    $test=0;
    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
        {
        $sql3 = "SELECT `code`,`noms`,`prenoms`,`utilisateurs` FROM `module-001-liste-employes` WHERE `code` LIKE '".$row2['codes']."'";
        $stmt3 = $dbh->query($sql3);
        while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) 
            {
            if($row3['code']==$row['responsables'])
                {
                $test=1;
                $html .='<option value="'.$row3['code'].'" selected>'.$row3['noms'].' '.$row3['prenoms'].'</option>';    
                }
            else
                {
                $html .='<option value="'.$row3['code'].'">'.$row3['noms'].' '.$row3['prenoms'].'</option>';   
                }
            }
        }
    if($test==0)
        {
        $html .='<option value="" selected>Non défini</option>';  
        }
    else
        {
        $html .='<option value="">Non défini</option>';  
        }
    $html .='</select></td>';

    
    $html .='<td class="coordo1"><select style="width:250px" class="form-select selectable" data-id="' . $row['id'] . '" data-field="coordo1">';
    $sql2 = "SELECT codes FROM `module-001-responsables-encadrement` WHERE `coordinateurs` LIKE 'true'";
    $stmt2 = $dbh->query($sql2);
    $test=0;
    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
        {
        $sql3 = "SELECT `code`,`noms`,`prenoms`,`utilisateurs` FROM `module-001-liste-employes` WHERE `code` LIKE '".$row2['codes']."'";
        $stmt3 = $dbh->query($sql3);
        while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) 
            {
            if($row3['code']==$row['coordo1'])
                {
                $test=1;
                $html .='<option value="'.$row3['code'].'" selected>'.$row3['noms'].' '.$row3['prenoms'].'</option>';    
                }
            else
                {
                $html .='<option value="'.$row3['code'].'">'.$row3['noms'].' '.$row3['prenoms'].'</option>';   
                }
            }
        }
    if($test==0)
        {
        $html .='<option value="" selected>Non défini</option>';  
        }
    else
        {
        $html .='<option value="">Non défini</option>';  
        }
    $html .='</select></td>';


    $html .='<td class="coordo2"><select style="width:250px" class="form-select selectable" data-id="' . $row['id'] . '" data-field="coordo2">';
    $sql2 = "SELECT codes FROM `module-001-responsables-encadrement` WHERE `coordinateurs` LIKE 'true'";
    $stmt2 = $dbh->query($sql2);
    $test=0;
    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
        {
        $sql3 = "SELECT `code`,`noms`,`prenoms`,`utilisateurs` FROM `module-001-liste-employes` WHERE `code` LIKE '".$row2['codes']."'";
        $stmt3 = $dbh->query($sql3);
        while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) 
            {
            if($row3['code']==$row['coordo2'])
                {
                $test=1;
                $html .='<option value="'.$row3['code'].'" selected>'.$row3['noms'].' '.$row3['prenoms'].'</option>';    
                }
            else
                {
                $html .='<option value="'.$row3['code'].'">'.$row3['noms'].' '.$row3['prenoms'].'</option>';   
                }
            }
        }
    if($test==0)
        {
        $html .='<option value="" selected>Non défini</option>';  
        }
    else
        {
        $html .='<option value="">Non défini</option>';  
        }
    $html .='</select></td>';



    $html .= '<td class="editable courriels" data-id="' . $row['id'] . '" data-field="courriels" contenteditable>' . $row['courriels'] . '</td>';
    $html .= '<td class="editable utilisateurs" data-id="' . $row['id'] . '" data-field="utilisateurs" contenteditable>' . $row['utilisateurs'] . '</td>';
    //$html .= '<td class="editable missions_rh" data-id="' . $row['id'] . '" data-field="missions_rh" contenteditable>'. preg_replace('/<[^>]*>/', '', $row['missions_rh']) .'</td>';
    $html .= '<td>';
    $html .= '<textarea class="editable missions_rh" data-id="' . $row['id'] . '" data-field="missions_rh" cols="60" rows="8">' . htmlspecialchars(preg_replace('/<[^>]*>/', '', $row['missions_rh'])) . '</textarea>';
    $html .= '</td>';


    $html .='<td class="id_secteurs"><select style="width:auto" class="form-select selectable" data-id="' . $row['id'] . '" data-field="id_secteurs">';
    $sql2 = "SELECT * FROM `module-001-secteurs`";
    $stmt2 = $dbh->query($sql2);
    while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) 
        {
        if($row2['id']==$row['id_secteurs'])
            {
            $html .='<option value="'.$row2['id'].'" selected>'.$row2['secteurs'].'</option>';    
            }
        else
            {
            $html .='<option value="'.$row2['id'].'">'.$row2['secteurs'].'</option>';  
            }
        }
    $html .='</select></td>';

    $html .='<td class="id_sites_principaux"><select style="width:200px" class="form-select selectable" data-id="' . $row['id'] . '" data-field="id_sites_principaux">';
    $sql3 = "SELECT * FROM `module-001-sites`";
    $stmt3 = $dbh->query($sql3);
    while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) 
        {
        if($row3['id_secteurs'] == $row['id_secteurs'])
            {
            if($row3['id']==$row['id_sites_principaux'])
                {
                $html .='<option value="'.$row3['id'].'" selected>'.$row3['sites'].'</option>';    
                }
            else
                {
                $html .='<option value="'.$row3['id'].'">'.$row3['sites'].'</option>';  
                }
            }
        }
    $html .='</select></td>';


    $html .= '<td class="id_sites_secondaires w-200"><div class="table2-container" style="white-space: nowrap;display:flex;align-items:flex-start;">';
    $html .= '<table style="display:inline-block;margin-right:25px;font-size:11px"><tr><th>DIEPPE</th></tr>';
    $sql3 = "SELECT * FROM `module-001-sites` WHERE id_secteurs LIKE '1' ORDER BY `module-001-sites`.`sites` ASC";
    $stmt3 = $dbh->query($sql3);
    while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) 
        {
        $parts = explode("|", $row['id_sites_secondaires']);
        $test=0;
        foreach ($parts as $part)
            {
            if($row3['id']==$part && $test==0)
                {
                $test=1;
                $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'" checked>'.$row3['sites'].'</label></td></tr>';    
                }
            }
        if($test==0)
            {
            $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'">'.$row3['sites'].'</label></td></tr>';  
            }
        }  
    $html .= '</table>';

    $html .= '<table style="display:inline-block;margin-right:25px;font-size:11px"><tr><th>EURE</th></tr>';
    $sql3 = "SELECT * FROM `module-001-sites` WHERE id_secteurs LIKE '2' ORDER BY `module-001-sites`.`sites` ASC";
    $stmt3 = $dbh->query($sql3);
    while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) 
        {
        $parts = explode("|", $row['id_sites_secondaires']);
        $test=0;
        foreach ($parts as $part)
            {
            if($row3['id']==$part && $test==0)
                {
                $test=1;
                $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'" checked>'.$row3['sites'].'</label></td></tr>';    
                }
            }
        if($test==0)
            {
            $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'">'.$row3['sites'].'</label></td></tr>';  
            }
        }  
    $html .= '</table>';
    
    $html .= '<table style="display:inline-block;margin-right:25px;font-size:11px"><tr><th>LE HAVRE</th></tr>';
    $sql3 = "SELECT * FROM `module-001-sites` WHERE id_secteurs LIKE '3' ORDER BY `module-001-sites`.`sites` ASC";
    $stmt3 = $dbh->query($sql3);
    while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) 
        {
        $parts = explode("|", $row['id_sites_secondaires']);
        $test=0;
        foreach ($parts as $part)
            {
            if($row3['id']==$part && $test==0)
                {
                $test=1;
                $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'" checked>'.$row3['sites'].'</label></td></tr>';    
                }
            }
        if($test==0)
            {
            $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'">'.$row3['sites'].'</label></td></tr>';  
            }
        }  
    $html .= '</table>';
    
    $html .= '<table style="display:inline-block;margin-right:25px;font-size:11px"><tr><th>ROUEN</th></tr>';
    $sql3 = "SELECT * FROM `module-001-sites` WHERE id_secteurs LIKE '4' ORDER BY `module-001-sites`.`sites` ASC";
    $stmt3 = $dbh->query($sql3);
    while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) 
        {
        $parts = explode("|", $row['id_sites_secondaires']);
        $test=0;
        foreach ($parts as $part)
            {
            if($row3['id']==$part && $test==0)
                {
                $test=1;
                $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'" checked>'.$row3['sites'].'</label></td></tr>';    
                }
            }
        if($test==0)
            {
            $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'">'.$row3['sites'].'</label></td></tr>';  
            }
        }  
    $html .= '</table>';
    
    $html .= '<table style="display:inline-block;margin-right:25px;font-size:11px"><tr><th>SIEGE</th></tr>';
    $sql3 = "SELECT * FROM `module-001-sites` WHERE id_secteurs LIKE '5' ORDER BY `module-001-sites`.`sites` ASC";
    $stmt3 = $dbh->query($sql3);
    while ($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) 
        {
        $parts = explode("|", $row['id_sites_secondaires']);
        $test=0;
        foreach ($parts as $part)
            {
            if($row3['id']==$part && $test==0)
                {
                $test=1;
                $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'" checked>'.$row3['sites'].'</label></td></tr>';    
                }
            }
        if($test==0)
            {
            $html .='<tr><td><input type="checkbox" class="checkbox" data-id="' . $row['id'] . '" data-field="id_sites_secondaires" id="checkbox'.$row['id'].'" name="checkbox'.$row['id'].'" value="'.$row3['id'].'">'.$row3['sites'].'</label></td></tr>';  
            }
        }  
    $html .= '</table>';

    $html .= '<td class="editable telephones_sites" data-id="' . $row['id'] . '" data-field="telephones_sites" contenteditable>' . $row['telephones_sites'] . '</td>';
    $html .= '<td class="editable telephones_poste" data-id="' . $row['id'] . '" data-field="telephones_poste" contenteditable>' . $row['telephones_poste'] . '</td>';
    $html .= '<td class="editable lignes_directes" data-id="' . $row['id'] . '" data-field="lignes_directes" contenteditable>' . $row['lignes_directes'] . '</td>';
    $html .= '<td class="editable telephones_portables" data-id="' . $row['id'] . '" data-field="telephones_portables" contenteditable>' . $row['telephones_portables'] . '</td>';

    $html .= '</div></td>';
    $html .= '</tr>';
}

$html .= '</table>';

$html .= '<script>function highlightRow(event) {
    var rows = document.querySelectorAll(".table tr");
    rows.forEach(function(row) {
        row.classList.remove("highlight");
    });
    if (event.target.tagName === "SELECT") {
        event.target.closest(\'tr\').classList.add("highlight");
    } else {
        event.currentTarget.classList.add("highlight");
    }
}

var rows = document.querySelectorAll(".table tr");
rows.forEach(function(row, index) {
    if (index !== 0) {
        row.addEventListener("click", highlightRow);
    }
});

var buttons = document.querySelectorAll(".table tr button, .table tr input[type=\'button\'], .table tr input[type=\'submit\']");
buttons.forEach(function(button) {
    button.addEventListener("click", function(event) {
        event.stopPropagation();
    });
});

var fileInputs = document.querySelectorAll(".table tr input[type=\'file\'].image-upload");
fileInputs.forEach(function(fileInput) {
    fileInput.addEventListener("click", function(event) {
        event.stopPropagation();
    });
});</script>';

echo $html;



?>