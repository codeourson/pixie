<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'librairies/PHPMailer-6.9.1/src/Exception.php';
require 'librairies/PHPMailer-6.9.1/src/PHPMailer.php';
require 'librairies/PHPMailer-6.9.1/src/SMTP.php';

function envoyerMailOutlook($sujet, $destinataire, $contenu) 
    {
    include './components/config.php';

    $mail = new PHPMailer;
    $mail->isSMTP();

    $mail->Host = $config['SMTP']['SMTP_HOST']; 
    $mail->SMTPAuth = true;
    $mail->Username = $config['SMTP']['SMTP_USERNAME'];
    $mail->Password = $config['SMTP']['SMTP_PASSWORD']; 
    $mail->SMTPSecure = $config['SMTP']['SMTP_SMTPSECURE'];
    $mail->Port = 587;

    // Paramètres de l'e-mail
    $mail->setFrom($config['SMTP']['SMTP_USERNAME'], $config['SMTP']['SMTP_TEAM']);
    $mail->addAddress($destinataire); // Adresse du destinataire*/
    $mail->Subject = $sujet;
    $mail->Body    = $contenu;

    // Configuration des en-têtes
    $mail->CharSet = 'UTF-8'; // Encodage des caractères
    $mail->Encoding = 'base64'; // Encodage du contenu
    $mail->isHTML(true); // Indiquer que le contenu est HTML

    // Envoyer l'e-mail
    if(!$mail->send()) 
        {
        echo 'Erreur lors de l\'envoi de l\'e-mail : ' . $mail->ErrorInfo;
        } 
    else 
        {
        echo 'E-mail envoyé avec succès';
        }
    }
?>