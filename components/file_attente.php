<?php
// Fonction pour insérer une ligne dans la table file_attente
function insererLigne($page, $utilisateur) 
    {   
    // Requête SQL pour insérer une nouvelle ligne
    $query = "INSERT INTO file_attente (page, utilisateur) VALUES (:page, :utilisateur)";
    $stmt = $bdd->prepare($query);
    $stmt->bindParam(':page', $page);
    $stmt->bindParam(':utilisateur', $utilisateur);
    $stmt->execute();
    } 

// Fonction pour supprimer une ligne de la table file_attente
function supprimerLigne($page, $utilisateur) 
    {
    // Requête SQL pour supprimer une ligne
    $query = "DELETE FROM file_attente WHERE page = :page AND utilisateur = :utilisateur";
    $stmt = $bdd->prepare($query);
    $stmt->bindParam(':page', $page);
    $stmt->bindParam(':utilisateur', $utilisateur);
    $stmt->execute();
    }

// Fonction pour tester si une ligne existe dans la table file_attente
function ligneExiste($page) 
    {
    // Requête SQL pour tester si une ligne existe
    $query = "SELECT COUNT(*) AS count FROM file_attente WHERE page = :page";
    $stmt = $bdd->prepare($query);
    $stmt->bindParam(':page', $page);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
    return $row['count'] > 0;
    }

// Exemples d'utilisation des fonctions
// Pour insérer une ligne
// insererLigne("Page1", "Utilisateur1");

// Pour supprimer une ligne
// supprimerLigne("Page1", "Utilisateur1");

// Exemple d'utilisation de la fonction
/*$page = "Page1";
if (ligneExiste($page)) {
    echo "La ligne pour la page $page existe.";
} else {
    echo "La ligne pour la page $page n'existe pas.";
}*/

?>