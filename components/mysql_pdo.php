<?php

include './components/config.php';

$dsn = 'mysql:host='.$config['MYSQL']['MYSQL_SERVER'].';dbname='.$config['MYSQL']['MYSQL_DATABASE'].'';
$username = $config['MYSQL']['MYSQL_USERNAME'];
$password = $config['MYSQL']['MYSQL_PASSWORD'];
$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',);
try 
    {
    $dbh = new PDO($dsn, $username, $password, $options);
    } 
catch (PDOException $e) 
    {
    die('Erreur de connexion : ' . $e->getMessage());
    }
?>