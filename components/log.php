<?php

$userid="Inconnu";
//Tentative d'accès forcé ?
if (isset($_SESSION['utilisateurs'])) 
	{
    $userid=$_SESSION['utilisateurs'];
	} 

// Récupération de l'adresse IP de l'utilisateur
$ip = $_SERVER['REMOTE_ADDR'];

// Récupération de la date et de l'heure actuelle
$date = date('Y-m-d');
$heure = date('H:i:s');

// Récupération du nom de la page
$page = basename($_SERVER['PHP_SELF']);

// Récupération du type de système d'exploitation
$user_agent = $_SERVER['HTTP_USER_AGENT'];
$os = "Inconnu"; // Valeur par défaut

// Détection du type de système d'exploitation
if (strpos($user_agent, 'Windows') !== false) 
    {
    $os = 'Windows';
    }
elseif (strpos($user_agent, 'Android') !== false) 
    {
    $os = 'Android';
    } 
elseif (strpos($user_agent, 'iOS') !== false) 
    {
    $os = 'iOS';
    }
elseif (strpos($user_agent, 'Macintosh') !== false) 
    {
    $os = 'Macintosh';
    } 
elseif (strpos($user_agent, 'Linux') !== false) 
    {
    $os = 'Linux';
    } 

// Concaténation des données à écrire dans le fichier
$data = "$userid | $ip | $date | $heure | $page | $os\n";

// Chemin du fichier dans lequel écrire les informations
$filename = './components/database/'.$date.'-log.csv';

// Ouverture du fichier en mode ajout (ou création s'il n'existe pas)
$file = fopen($filename, 'a');

// Vérification si l'ouverture du fichier est réussie
if ($file) 
    {
    // Écriture des données dans le fichier
    fwrite($file, $data);
    
    // Fermeture du fichier
    fclose($file);
    }
?>