    <!-- Sidebar Start -->
    <aside class="left-sidebar">
      <!-- Sidebar scroll-->
      <div>
        <div class="brand-logo d-flex align-items-center justify-content-between">
          <div>
            <a href="./index.html" class="text-nowrap logo-img">
              <img src="../assets/images/logos/logo2.png" height="80" alt="" />
            </a>
          </div>
          <div class="close-btn d-xl-none d-block sidebartoggler cursor-pointer" id="sidebarCollapse">
            <i class="ti ti-x fs-8"></i>
          </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav scroll-sidebar" data-simplebar="">
          <ul id="sidebarnav">
            <?php
              if($page = basename($_SERVER['PHP_SELF'])=="pixie_app_001_lebottin.php")
                {
                echo '<li class="nav-small-cap">';
                echo '<i class="ti ti-dots nav-small-cap-icon fs-4"></i>';
                echo '<span class="hide-menu">LE BOTTIN</span>';
                echo '</li>'; 
                
                ?>

                <form id="searchForm">
                    <label for="typeDossier">Type de dossier :</label>
                    <select id="typeDossier" name="typeDossier">
                        <option value="">Tous</option>
                        <?php
                            // Récupérer les types de dossier distincts depuis la base de données
                            $query = "SELECT DISTINCT type_de_dossier FROM `module-004-suivi-dossier`";
                            $stmt = $dbh->query($query);

                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                echo "<option value='" . $row['type_de_dossier'] . "'>" . $row['type_de_dossier'] . "</option>";
                            }
                        ?>
                    </select>

                    <br>
                    <label for="organismeFormation">Organisme de formation :</label>
                    <select id="organismeFormation" name="organismeFormation">
                        <option value="">Tous</option>
                        <?php
                            // Récupérer les organismes de formation distincts depuis la base de données
                            $query = "SELECT DISTINCT organisme_formation FROM `module-004-suivi-dossier`";
                            $stmt = $dbh->query($query);

                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                echo "<option value='" . $row['organisme_formation'] . "'>" . $row['organisme_formation'] . "</option>";
                            }
                        ?>
                    </select>

                    <br>
                    <label for="nomStagiaire">Nom du stagiaire :</label>
                    <select id="nomStagiaire" name="nomStagiaire">
                        <option value="">Tous</option>
                        <?php
                            // Récupérer les noms des stagiaires distincts depuis la base de données
                            $query = "SELECT DISTINCT nom FROM `module-004-suivi-dossier`";
                            $stmt = $dbh->query($query);

                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                echo "<option value='" . $row['nom'] . "'>" . $row['nom'] . "</option>";
                            }
                        ?>
                    </select>
                </form>

                <script>
                    $(document).ready(function(){
                        $('#searchForm select').change(function(){
                            var formData = $('#searchForm').serialize();
                            $.ajax({
                                type: 'POST',
                                url: 'pixie_app_001_lebottin.php', // Chemin vers le script PHP qui traitera la requête AJAX
                                data: formData,
                                success: function(data){
                                    $('#resultat').html(data);
                                }
                            });
                        });

                        // Au chargement de la page, vérifie s'il y a des données POST
                        // Si non, affiche toutes les entrées de la table
                        $(window).on('load', function() {
                            if (!$.trim($('#resultat').html())) {
                                $.ajax({
                                    type: 'POST',
                                    url: 'pixie_app_001_lebottin.php',
                                    success: function(data) {
                                        $('#resultat').html(data);
                                    }
                                });
                            }
                        });
                    });
                </script>

                <?php
                }

              if(isset($_SESSION['utilisateurs']) && !empty($_SESSION['utilisateurs'])) 
              {
              echo '<li class="nav-small-cap">';
              echo '<i class="ti ti-dots nav-small-cap-icon fs-4"></i>';
              echo '<span class="hide-menu">MENU</span>';
              echo '</li>';

              echo '<li class="sidebar-item">';
              echo '<a class="sidebar-link" href="'.$config['PIXIE']['DOSSIER_PIXIE'].'/pixie.php" aria-expanded="false">';
              echo '<span>';
              echo '<i class="ti ti-layout-dashboard"></i>';
              echo '</span>';
              echo '<span class="hide-menu">Accueil</span>';
              echo '</a>';
              echo '</li>';
              echo '<li class="sidebar-item">';
              echo '<a class="sidebar-link" href="'.$config['PIXIE']['DOSSIER_PIXIE'].'/pixie_app_001_lebottin.php" aria-expanded="false">';
              echo '<span>';
              echo '<i class="ti ti-layout-dashboard"></i>';
              echo '</span>';
              echo '<span class="hide-menu">Le Bottin</span>';
              echo '</a>';
              echo '</li>';
                

                echo '<li class="nav-small-cap">';
                echo '<i class="ti ti-dots nav-small-cap-icon fs-4"></i>';
                echo '<span class="hide-menu">ADMI</span>';
                echo '</li>';

                $sql3 = 'SELECT * FROM utilisateurs WHERE `utilisateur` LIKE "' .$_SESSION['utilisateurs']. '"'; ;
                $stmt = $dbh->query($sql3);
                $allows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($allows as $allow)
                  {
                  //Autorisation Export Avenir
                  if ($allow["exportavenir"]==1)
                      {
                      echo '<li class="sidebar-item">';
                      echo '<a class="sidebar-link" href="'.$config['PIXIE']['DOSSIER_PIXIE'].'/pixie_conversion_export_2024avenir.php?mode=1" aria-expanded="false">';
                      echo '<span>';
                      echo '<i class="ti ti-layout-dashboard"></i>';
                      echo '</span>';
                      echo '<span class="hide-menu">Export #AVENIR</span>';
                      echo '</a>';
                      echo '</li>';
                      echo '<li class="sidebar-item">';
                      echo '<a class="sidebar-link" href="'.$config['PIXIE']['DOSSIER_PIXIE'].'/pixie_recherche_export_2024avenir.php" aria-expanded="false">';
                      echo '<span>';
                      echo '<i class="ti ti-layout-dashboard"></i>';
                      echo '</span>';
                      echo '<span class="hide-menu">Emargement #AVENIR</span>';
                      echo '</a>';
                      echo '</li>';
                      }
                  if ($allow["exportrtt"]==1)
                      {
                      echo '<li class="sidebar-item">';
                      echo '<a class="sidebar-link" href="'.$config['PIXIE']['DOSSIER_PIXIE'].'/pixie_conversion_export_rtt.php?mode=1" aria-expanded="false">';
                      echo '<span>';
                      echo '<i class="ti ti-layout-dashboard"></i>';
                      echo '</span>';
                      echo '<span class="hide-menu">Export #RTT</span>';
                      echo '</a>';
                      echo '</li>';
                      }
                  if ($allow["administrateur"]==1)
                      {
                      echo '<li class="nav-small-cap">';
                      echo '<i class="ti ti-dots nav-small-cap-icon fs-4"></i>';
                      echo '<span class="hide-menu">AUTHENTIFICATION</span>';
                      echo '</li>';
                      echo '<li class="sidebar-item">';
                      echo '<a class="sidebar-link" href="'.$config['PIXIE']['DOSSIER_PIXIE'].'/creation_utilisateur.php" aria-expanded="false">';
                      echo '<span>';
                      echo '<i class="ti ti-settings"></i>';
                      echo '</span>';
                      echo '<span class="hide-menu">Créer un utilisateur</span>';
                      echo '</a>';
                      echo '</li>';
                      }
                  }
                echo '<li class="sidebar-item">';
                echo '<a class="sidebar-link" href="'.$config['PIXIE']['DOSSIER_PIXIE'].'/logout.php" aria-expanded="false">';
                echo '<span>';
                echo '<i class="ti ti-login"></i>';
                echo '</span>';
                echo '<span class="hide-menu">Se déconnecter</span>';
                echo '</a>';
                echo '</li>';
                }
          ?>
          </ul>
        </nav>
        <!-- End Sidebar navigation -->
      </div>
      <!-- End Sidebar scroll-->
    </aside>
    <!--  Sidebar End -->