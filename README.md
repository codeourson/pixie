# Pixie

## Description

Framework open-source de mon template Web universel

## Modules disponibles 

- auth.php => Authentification avec SimpleSAML (ou traditionnel en commentaire)
- config.php => Permet de récupérer les variables d'environnement du framework dans la page courante
- content.php => Haut générique du body
- error.php => Permet d'activer l'affichage des erreurs PHP (pour le debug)
- file_attente.php => Pour la gestions des files attentes dans un process MYSQL (pour eviter un double import de données par exemple dans un projet) (EN TEST)
- footer.php => Bas de page générique de la page Web
- header.php => En-tête générique de la page Web (paramètre $title disponible)
- log.php => Pour suivre les accès des pages du site Web
- mail.php => Pour envoyer un mail avec PHPMailer (fonction disponible : envoyerMailOutlook($sujet, $destinataire, $contenu) )
- mysql_pdo.php => Pour initialiser le serveur MYSQL du site Web
- navbar.php => Pour créer dynamiquement la barre de navigation du site Web
- scriptjs.php => Pour initaliser les scripts JS utilisés en début de page
- template.php => Exemple de page WEB

## Fichier de configuration

- components/config.ini => Permet d'initialiser les environnements du projet et modules